//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//MVC - > MODEL VIEW CONTROLLER.

//MODELO: 


var numero: Int = 6

var numeroString: String = "\(numero) es mi numero preferido \(67)"
var stringConcatenado = numeroString + " el papa de alguien."
//print(numeroString)
//print(stringConcatenado)

//---
// null -> nil
var nombre: String? = "Kevin"
nombre = nil
nombre = "jaime"

//Forma numero 1: - No Segura.
print(nombre!)

//Forma numero 1: - Segura.
if let nombreSeguro = nombre {
    //Si entra aca es porque nombre, tenia un valor string.
    print(nombreSeguro)
} else {
    //Si entra aca es porque nombre, tenia nil.
}

//Forma numero 2: - Segura.
func saludarSiempreYCuandoTengaNombre(nombre: String?) {
    
    guard let nombreSeguro = nombre else { return }
    
    
    print(nombreSeguro)
}

if let nombreCapitalizado = nombre?.capitalized {
        print(nombreCapitalizado)
} else {
    
}

struct Persona {
    var altura: Double?
}

let personaPepe = Persona(altura: nil)
let personaJuan = Persona(altura: 1.74)

let arrayPerrsonas: [Persona?] = [personaPepe, personaJuan, nil, nil]

for persona in arrayPerrsonas {
    if let personaNoNil = persona {
        if let alturaNoNil = personaNoNil.altura {
            alturaNoNil
        }
    }
}

for persona in arrayPerrsonas {
    if let alturaNoNil = persona?.altura {
        //Aca tenes alturas.
    } else {
        //Cae aca.
    }
}

var numeroOpcionalPorUnTiempo: Int!
//Un calculo de cosas...
numeroOpcionalPorUnTiempo = 4

print(numeroOpcionalPorUnTiempo)
numeroOpcionalPorUnTiempo = nil


// Formas de manejar datos.

// Array.
let array = [1,3,5,6]
print(array[0])

// Diccionarios.
var diccionary = [String:Int]()
diccionary["Pepe"] = 4

print(diccionary["Pepe"])

// Set.
let set = Set([1,4,4,5])
print(set)

let arraySeCreoDelSet = Array(set)
print(set)

// Tuplas.
let tupla = (1, "Nombre", 1.7)

func obtenerRespuesta() -> (Int, String, Double) {
    let tupla = (1, "Nombre", 1.7)
    return tupla
}

let respuesta = obtenerRespuesta()
print(respuesta.0)

let (id, nombreUsuario, puntajeWeb) = obtenerRespuesta()
print(id)
print(nombreUsuario)

let arrayTuplas = [(Int, String)]()










